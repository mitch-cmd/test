#!/bin/bash

# ############################################################
# # MAIN                                                     #
# ############################################################
        
sudo apt-get update  # To get the latest package lists

# VIM
sudo apt install vim -y

# SSH
sudo apt-get install openssh-server -y

# VNC
sudo apt-get install x11vnc -y
mkdir ~/.vnc    # directory for password storage
x11vnc -storepasswd parsnip ~/.vnc/x11pass.vnc

# LIGHTDM - FOR KIOSK
sudo apt-get install lightdm

# GIT
sudo apt-get install git -y

# CHECK AND CREATE KEY PAIR
if [ -f ~/.ssh/id_rsa.pub ];
then
    echo "Public key exists"
else
    echo "No public key exists - creating key"
    echo
    echo "### Creating SSH KEY PAIR ### "
    echo
    echo "Press enter to following prompts to resume key generation"
    echo
    ssh-keygen -o -t rsa -C "parsnipking1@gmail.com”
    echo "Key pair created"
    echo
fi

echo "### COPY BELOW KEY ### "
cat ~/.ssh/id_rsa.pub
echo "### END OF KEY ###"
echo
echo "Paste public key into github account"
echo "User perferences > SSH Keys"
echo
read -p "Once SSH Key saved press any key to RESUME"
echo "### BUILD RESUME ###"

if [ -d ~/vege ]; 
then
    echo "The vege repo is already cloned"
else
    echo "Cloning vege repo..."
    git clone git@gitlab.com:rnd-vision/vege.git
    git config --global user.email "parsnipking1@gmail.com"
    git config --global user.name "Par Snip"
fi

# SET ENV VARIABLES
cd ~/vege/
git checkout origin/ammar
chmod +x set-env.sh
source ./set-env.sh

# SET STATIC IP
echo "Setting static IP to $USER_HOST in /etc/netplan/01-network-manager-all.yaml"
sudo bash -c "echo 'network:
  version: 2
  renderer: NetworkManager
  ethernets:
    enp0s1:
      dhcp4: no
      addresses: [$USER_HOST/24]
      gateway4: 192.168.250.1' > /etc/netplan/01-network-manager-all.yaml"
sudo netplan apply

# INSTALL BACKEND
cd ~/vege/backend
git fetch origin
git checkout origin/ammar
./install.sh

# INSTALL FRONTEND
cd ~/vege/frontend
chmod +x install.sh
./install.sh
echo
